//
//  LoginViewModel.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import Foundation
import Combine

/// The ViewModel for the login screen
final class LoginViewModel: ObservableObject {

    // MARK: - DI
    typealias Dependencies = HasLoginService & HasKeyChainService
    private let dependencies: Dependencies

    // MARK: - Input
    @Published var login = ""
    @Published var password = ""

    // MARK: - Output
    @Published private(set) var isLoading = false
    @Published private(set) var isFormValid = false

    // MARK: - iVar | Internal
    private var subscriptions = Set<AnyCancellable>()
    private var userStatus: UserStatus

    // MARK: - Init
    init(userStatus: UserStatus, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.userStatus = userStatus

        // Validate input form
        Publishers.CombineLatest(self.$login, self.$password).map { (tuple) in

            let login = tuple.0
            let password = tuple.1

            return login.count > 3 && password.count > 4
        }.assign(to: &self.$isFormValid)
    }

    // MARK: - Actions
    func login(username: String, password: String) {

        self.isLoading = true
        self.dependencies.loginService.login(username: username, password: password)
            .tryMap { _ in try self.dependencies.keychain.add(username: username, password: password) }
            .delay(for: .seconds(2), scheduler: DispatchQueue.main)
            .sink { result in
                self.isLoading = false
                switch result {
                case .failure(let error):
                    print("Error: \(error)")
                case .finished:
                    print("Successfully logged")
                    self.userStatus.setUserCredential()
                }
            } receiveValue: { }.store(in: &self.subscriptions)


    }
}
