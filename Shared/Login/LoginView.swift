//
//  LoginView.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import SwiftUI

/// The view to log the user in
struct LoginView: View {

    // MARK: - iVar
    @ObservedObject var model: LoginViewModel

    // MARK: - Init
    init(userStatus: UserStatus) {
        self.model = LoginViewModel(userStatus: userStatus, dependencies: AppDependencies.shared)
    }

    // MARK: - Body
    var body: some View {
        
        VStack(spacing: 20.0) {

            Image("logo")
                .resizable()
                .scaledToFit()
                .frame(width: 300)
                .padding()

            Group {
                TextField("Username", text: $model.login)
                    .modifier(CommonTextField())

                SecureField("Password", text: $model.password)
                    .modifier(CommonTextField())
            }
            .opacity(self.model.isLoading ? 0.5 : 1.0)
            .disabled(self.model.isLoading)

            ZStack {
                if model.isLoading {
                    ProgressView()
                } else {
                    Button(action: {
                        self.model.login(username: model.login, password: model.password)
                    }) {
                        Text("Login")
                    }.buttonStyle(CommonButton())
                    .disabled(!self.model.isFormValid)
                }
            }
        }
        .padding()
    }
}

struct Login_Previews: PreviewProvider {

    static var previews: some View {
        LoginView(userStatus: UserStatus()).preferredColorScheme(.dark)

        LoginView(userStatus: UserStatus()).preferredColorScheme(.light)
    }
}

