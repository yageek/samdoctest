//
//  ListView.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import SwiftUI

struct ListView: View {

    // MARK: - Inputs
    @StateObject var model = ListViewModel(dependencies: AppDependencies.shared, repositories: [])

    @State var selectedRepository: Repository?

    // MARK: - Views
    var reloadView: some View {
        HStack {
            Spacer()
            ProgressView().onAppear(perform: {
                 self.model.loadNextPage()
             })
            Spacer()
        }

    }

    var endListView: some View {
        HStack {
            Spacer()
            Text("🎉")
            Spacer()
        }
    }

    var itemListView: some View {
        List {
            ForEach(model.repositories) { repo in
                ListItem(repository: repo).onTapGesture {                    
                    self.selectedRepository = repo
                }
            }
            if model.hasNextPageToFetch {
                self.reloadView
            } else {
                self.endListView
            }
        }.listStyle(PlainListStyle())
        .sheet(item: self.$selectedRepository) { (repo) in
            ItemDetail(repository: repo) {
                self.selectedRepository = nil
            }
        }
    }

    var emptyView: some View {
        VStack {
            Image("Octocat")
                .resizable()
                .scaledToFit()
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Text("No result found").font(.subheadline)
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
    }
    
    var errorView: some View {
        HStack {
            Image(systemName: "xmark.octagon.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .foregroundColor(.red)

            Text("Tap to try again").font(.subheadline)
            }.onTapGesture {
                self.model.loadNextPage()
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                if self.model.hasErrorOccured && self.model.repositories.isEmpty {
                    self.errorView.frame(maxWidth: .infinity, maxHeight: .infinity)
                } else if model.repositories.isEmpty && !self.model.isLoading {
                    self.emptyView
                } else if self.model.hasErrorOccured {
                    self.errorView
                } else if self.model.repositories.isEmpty && self.model.isLoading {
                    ProgressView()
                } else {
                    self.itemListView
                }
            }.navigationTitle("Search repository")
            .navigationBarSearch($model.searchQuery)
        }
    }
}

// MARK: - Preview
struct ListView_Previews: PreviewProvider {

    static let mockrepositories: [Repository] = [
        Repository(id: 1, name: "Repo #1", stars: 10, author: "Foo Bar"),
        Repository(id: 2, name: "Repo #2", stars: 20, author: "John Applefeed"),
        Repository(id: 3, name: "Repo #3", stars: 5, author: "Foo Bar"),
        Repository(id: 4, name: "Repo #4", stars: 100, author: "Foo Bar"),
        Repository(id: 5, name: "Repo #1", stars: 10, author: "Foo Bar"),
        Repository(id: 6, name: "Repo #2", stars: 20, author: "John Applefeed"),
        Repository(id: 7, name: "Repo #3", stars: 5, author: "Foo Bar"),
        Repository(id: 8, name: "Repo #4", stars: 100, author: "Foo Bar"),
        Repository(id: 9, name: "Repo #1", stars: 10, author: "Foo Bar"),
        Repository(id: 10, name: "Repo #2", stars: 20, author: "John Applefeed"),
        Repository(id: 11, name: "Repo #3", stars: 5, author: "Foo Bar"),
        Repository(id: 12, name: "Repo #4", stars: 100, author: "Foo Bar"),
    ]

    static var previews: some View {
        ListView(model: ListViewModel(dependencies: AppMockedDependencies.shared, repositories: mockrepositories, hasNextPageToFetch: false, hasErrorOccured: true))

        ListView(model: ListViewModel(dependencies: AppMockedDependencies.shared, repositories: [], hasNextPageToFetch: false))

        ListView(model: ListViewModel(dependencies: AppMockedDependencies.shared, repositories: mockrepositories, hasNextPageToFetch: false))
    }
}
