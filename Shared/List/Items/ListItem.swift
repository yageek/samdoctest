//
//  ListItem.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import SwiftUI

/// The item representing a repo line
struct ListItem: View {
    let repository: Repository
    var body: some View {

        VStack(alignment: .leading, spacing: 10.0) {

            Text("\(repository.author)/\(repository.name)")
                .font(.body)
                .fontWeight(.bold)
                .lineLimit(1)

            Text(repository.description)
                .font(.subheadline)
                .fontWeight(.light)
                .lineLimit(3)

            HStack {
                HStack(alignment: .firstTextBaseline) {
                    Image(systemName: "star.fill").foregroundColor(.yellow)
                    Text("\(repository.stars)").font(.footnote)
                        .fontWeight(.light)
                }
                Spacer()
                HStack(spacing: 2.0) {
                    Text(repository.license)
                        .font(.footnote)
                    
                    Image(systemName: "rosette")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 10)
                }
            }
        }.padding()
    }
}

// MARK: - Preview
struct ListItem_Previews: PreviewProvider {
    static let repository = Repository(id: 1, name: "Super project", stars: 1000, author: "Me", description: "An awesome project written in Swift")
    static var previews: some View {
        ListItem(repository: self.repository).padding()
    }
}
