//
//  ItemDetail.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 11.04.21.
//

import SwiftUI

/// Item details
struct ItemDetail: View {

    let repository: Repository
    var closeAction: (() -> Void)?
    var body: some View {

        VStack(alignment: .leading, spacing: 10.0) {

            Text(repository.name)
                .font(.title)
                .fontWeight(.bold)
                .lineLimit(1)

            Text(repository.description)
                .font(.subheadline)
                .fontWeight(.light)

            HStack(alignment: .firstTextBaseline) {
                Image(systemName: "person.crop.circle").foregroundColor(.primary)
                Text(repository.author).font(.footnote)
            }

            HStack(alignment: .firstTextBaseline) {
                Image(systemName: "star.fill").foregroundColor(.yellow)
                Text("\(repository.stars)").font(.footnote)
                    .fontWeight(.light)
                    .padding(.trailing, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)

                Image(systemName: "eye.fill").foregroundColor(.primary)
                Text("\(repository.stars)").font(.footnote)
                    .fontWeight(.light)
                Spacer()
            }

            HStack(alignment: .firstTextBaseline) {
                Image(systemName: "rosette")
                Text(repository.license)
                    .font(.footnote)
            }

            HStack(alignment: .firstTextBaseline) {
                Image(systemName: "exclamationmark.circle").foregroundColor(.primary)
                Text("\(repository.issues) bug(s)").font(.footnote)
            }

            HStack(alignment: .firstTextBaseline) {
                Image(systemName: "arrow.triangle.branch").foregroundColor(.primary)
                Text("\(repository.forks) fork(s)").font(.footnote)
            }.padding(.bottom, 30)

            Button(action: {
                self.closeAction?()
            }) {
                Text("Fermer")
                    .frame(maxWidth: .infinity)
            }.buttonStyle(CommonButton())

            Spacer()
        }.padding()

    }
}

// MARK: - Preview
struct ItemDetail_Previews: PreviewProvider {

    static var previews: some View {
        ItemDetail(repository: Repository(id: 1, name: "Name", stars: 2, author: "me"))
    }
}
