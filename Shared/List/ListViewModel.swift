//
//  ListViewModel.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import Foundation
import Combine


final class ListViewModel: ObservableObject {

    typealias Dependencies = HasGithubAPIService

    private let dependencies: Dependencies

    // MARK: - iVars | Outputs
    @Published private(set) var repositories: [Repository]
    @Published private(set) var hasNextPageToFetch: Bool
    @Published private(set) var hasErrorOccured: Bool
    @Published private(set) var isLoading: Bool = false
    
    // MARK: - iVars | Inputs
    @Published var searchQuery: String = ""
    private var currentPage: Int = 1
    private var subscriptions: Set<AnyCancellable> = Set()

    // MARK: - Init
    init(dependencies: Dependencies, repositories: [Repository], hasNextPageToFetch: Bool = false, hasErrorOccured: Bool = false) {
        self.dependencies = dependencies
        self.repositories = repositories
        self.hasNextPageToFetch = hasNextPageToFetch
        self.hasErrorOccured = hasErrorOccured

        // Not empty
        $searchQuery
            .handleEvents(receiveOutput: { (_) in
                self.isLoading = true
            })
            .debounce(for: .milliseconds(600), scheduler: DispatchQueue.global(qos: .userInitiated))
            .map { self.dependencies.apiService.search(query: $0, page: 1, count: 10)
                .map(ListViewModel.repositories(from:))
                .map { Result<([Repository], Int), Error>.success($0) }
                .catch { Just(Result<([Repository], Int), Error>.failure($0)) }
            }
            .switchToLatest()
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { (result) in
                self.isLoading = false
                switch result {
                case .success(let tuple):
                    self.hasErrorOccured = false
                    self.repositories = tuple.0
                    self.hasNextPageToFetch = self.repositories.count < tuple.1
                case .failure(let error):
                    print("Error: \(error)")
                    self.hasErrorOccured = true
                }
            }).store(in: &self.subscriptions)
    }

    // MARK: - Loading
    func loadNextPage() {

        guard self.hasNextPageToFetch else { return }

        self.dependencies.apiService.search(query: self.searchQuery, page: self.currentPage+1, count: 10)
            .map(ListViewModel.repositories(from:))
            .map { Result<([Repository], Int), Error>.success($0) }
            .catch { Just(Result<([Repository], Int), Error>.failure($0)) }
            .receive(on: DispatchQueue.main)
            .sink { (result) in
                switch result {
                case .failure(let error):
                    print("Error: \(error)")
                    self.hasErrorOccured = true
                case .success(let tuple):
                    let newElements =  self.repositories + tuple.0
                    self.repositories = newElements
                    self.currentPage += 1
                    self.hasNextPageToFetch = newElements.count < tuple.1
                    self.hasErrorOccured = false
                }
            }.store(in: &self.subscriptions)
    }

    // MARK: - Helpers
    static func repositories(from searchResult: GithubSearchResult) -> ([Repository], Int) {
        let items = searchResult.items.map { Repository(from: $0) }
        return (items, searchResult.totalCount)
    }
}
