//
//  VRPermissionView.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 12.04.21.
//

import SwiftUI

/// The view asking the permission for the camera
struct VRPermissionView: View {

    // MARK: - View
    @ObservedObject var model = VRPermissionViewModel()
    @Environment(\.openURL) var openURL
    @State var isVrPresented: Bool = false
    var askPermissionView: some View {
        Group {
            Text("Pour lancer l'expérience vous devez autorisé l'accès à la caméra")
                .font(.body)
                .frame(height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
                .padding()

            Button(action: {
                self.model.askPermissions()
            }, label: {
                Text("Autoriser l'accès à la caméra")
            }).buttonStyle(CommonButton())
            .padding()
        }
    }

    var mainView: some View {
        Button(action: {
            isVrPresented.toggle()
        }, label: {
            Text("Lancer l'experience")
        }).buttonStyle(CommonButton())
    }

    var deniedView: some View {

        Group {
            Text("L'application n'autorise pas la caméra").padding()

            Button(action: {
                self.model.openSettings()
            }, label: {
                Text("Ouvrir les paramètres")
            }).buttonStyle(CommonButton())
        }
    }

    var body: some View {

        VStack {

            Text("VR Experience").font(.title2).fontWeight(.semibold).padding()

            switch model.status {
            case .authorized:
                 mainView.fullScreenCover(isPresented: $isVrPresented) {
                    VRView()
                 }
            case .denied:
                self.deniedView
            case .unknown:
                self.askPermissionView
            case .notSupported:
                Text("Cet appareil ne peut pas afficher l'expérience")
            }
        }
    }
}

// MARK: - Preview
struct VRPermissionView_Previews: PreviewProvider {
    static var previews: some View {
        VRPermissionView(model: VRPermissionViewModel(status: .notSupported))
        VRPermissionView(model: VRPermissionViewModel(status: .authorized))
        VRPermissionView(model: VRPermissionViewModel(status: .denied))
        VRPermissionView(model: VRPermissionViewModel(status: .unknown))
    }
}
