//
//  VRPermissionViewModel.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 12.04.21.
//

import Combine
import AVFoundation
import ARKit
/// The `ViewModel` for the `VRPermissionViewModel`
final class VRPermissionViewModel: ObservableObject {

    enum Status {
        case authorized
        case unknown
        case denied
        case notSupported
    }
    // MARK: - Outputs
    @Published private(set) var status: Status

    // MARK: - init

    /// :nodoc:
    init(status: Status = .unknown) {
        self.status = status
        self.reloadStatus()
    }

    private func reloadStatus() {

        guard ARFaceTrackingConfiguration.isSupported else {
            self.status = .notSupported
            return
        }
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                self.status = .authorized
            case .notDetermined: // The user has not yet been asked for camera access.
                self.status = .unknown
            case .denied, .restricted: // The user can't grant access due to restrictions.
                self.status = .denied
        @unknown default:
            self.status = .denied
        }
    }

    // MARK: - Public API
    func askPermissions() {
        AVCaptureDevice.requestAccess(for: .video) { (access) in
            DispatchQueue.main.async {
                if access {
                    self.status = .authorized
                } else {
                    self.status = .denied
                }
            }
        }
    }

    func openSettings() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
}
