//
//  ARKitViewController.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 11.04.21.
//

import UIKit
import ARKit
import SwiftUI
import SceneKit

// MARK: - Delegate
/// Delegate for the `ARKitViewController`
protocol ARKitViewControllerDelegate: AnyObject {

    /// Notify the delegate the close button has been triggered
    /// - Parameter controller: The `ARKitViewController` instances triggering the event
    func arkitViewControllerCloseButtonTriggered(_ controller: ARKitViewController)
}

// MARK: - Class
final class ARKitViewController: UIViewController, ARSCNViewDelegate {

    // MARK: - iVars
    private var vrView: ARSCNView!
    private var faceNode: SCNNode?
    private var emojiNode = SCNNode()
    private var emojiGeometry = SCNPlane(width: 0.30, height: 0.30)

    weak var delegate: ARKitViewControllerDelegate?

    // MARK: - Init

    /// :nodoc:
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("not used")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = ARSCNView()
        view.delegate = self
        self.view = view
        self.vrView = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "VR Experience"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(closeTriggered(_:)))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let configuration = ARFaceTrackingConfiguration()
        self.vrView.session.run(configuration)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.vrView.session.pause()
    }

    // MARK: - Action
    @objc private func closeTriggered(_ sender: Any) {
        self.delegate?.arkitViewControllerCloseButtonTriggered(self)
    }

    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {

        if let device = self.vrView.device, anchor is ARFaceAnchor {

            let geometry = ARSCNFaceGeometry(device: device)
            let faceNode = SCNNode(geometry: geometry)
            faceNode.geometry?.firstMaterial?.transparency = 0.0

            self.emojiGeometry.firstMaterial?.diffuse.contents = UIImage(named: "emoji")
            self.emojiNode.geometry = self.emojiGeometry
            self.emojiNode.position = faceNode.position

            faceNode.addChildNode(self.emojiNode)

            self.faceNode = faceNode
            return faceNode
        }
        return nil
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let anchor = anchor as? ARFaceAnchor, let geometry = node.geometry as? ARSCNFaceGeometry else { return }
        geometry.update(from: anchor.geometry)
    }
}


// MARK: - SwiftUI compatibility
struct VRView: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode

    class Coordinator: NSObject, ARKitViewControllerDelegate {
        let mode: Binding<PresentationMode>
        init(mode: Binding<PresentationMode>) {
            self.mode = mode
        }

        func arkitViewControllerCloseButtonTriggered(_ controller: ARKitViewController) {
            self.mode.wrappedValue.dismiss()
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(mode: self.presentationMode)
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) { }

    func makeUIViewController(context: Context) -> some UIViewController {
        let controller = ARKitViewController()
        controller.delegate = context.coordinator
        let nav = UINavigationController(rootViewController: controller)
        return nav
    }
}
