//
//  APIService.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 10.04.21.
//

import Foundation
import Combine

// MARK: - Service
protocol GithubAPIService {

    /// Search repositories
    /// - Parameters:
    ///   - query: The query
    ///   - page: The page to fetch
    ///   - count: The number of items per page
    func search(query: String, page: Int, count: Int) -> AnyPublisher<GithubSearchResult, Error>
}

// MARK: - DI
protocol HasGithubAPIService {
    var apiService: GithubAPIService { get }
}

// MARK: - Impl
extension GithubAPIClient: GithubAPIService {}

// MARK: - Mock
struct MockGithubService: GithubAPIService {
    private struct FakeError: Error {}
    func search(query: String, page: Int, count: Int) -> AnyPublisher<GithubSearchResult, Error> {
        return Fail(error: FakeError()).eraseToAnyPublisher()
    }
}
