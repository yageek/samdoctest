//
//  LoginService.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import Foundation
import Combine


/// The kind of error returned by the service
enum LoginServiceError: Error {
    case invalidCredential
    case network(Error)
}

// MARK: - Service
/// The service responsible to log the user
protocol LoginService {

    /// Log the user
    /// - Parameters:
    ///   - username: The username of the user
    ///   - password: The password of the user
    func login(username: String, password: String) -> AnyPublisher<(), LoginServiceError>
}

/// DI for `LoginService`
protocol HasLoginService {
    /// :nodoc:
    var loginService: LoginService { get }
}

// MARK: - Mock

///:nodoc:
struct MockLoginService: LoginService {

    let result: Result<(), LoginServiceError>

    init(result: Result<(), LoginServiceError>) {
        self.result = result
    }

    ///:nodoc:
    func login(username: String, password: String) -> AnyPublisher<(), LoginServiceError> {

        return Future { promise in
            promise(self.result)
        }
        .delay(for: .milliseconds(800), scheduler: DispatchQueue.global(qos: .utility))
        .eraseToAnyPublisher()
    }
}
