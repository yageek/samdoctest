//
//  KeychainService.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import Foundation

// MARK: - Service Error
enum KeychainError: Error {
    case noPassword
    case unexpectedPasswordData
    case unhandledError(status: OSStatus)
}

// MARK: - Service
/// The service handling the keychain
protocol KeychainService {

    /// Add the key into the keychain
    /// - Parameters:
    ///   - username: The username to add
    ///   - password: The password to add
    func add(username: String, password: String) throws
}

// MARK: - DI for `HasKeyChainService`
protocol HasKeyChainService {
    var keychain: KeychainService { get }
}

// MARK: - Mock
struct MockKeyChainService: KeychainService {
    func add(username: String, password: String) throws { }
}

// MARK: - Implementation
///:nodoc:
final class Keychain: KeychainService {

    ///:nodoc:
    static let serverKey = "doc.sam"

    ///:nodoc:
    func add(username: String, password: String) throws {
        let account = username
        let password = password.data(using: .utf8)!

        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                    kSecAttrAccount as String: account,
                                    kSecAttrServer as String: Keychain.serverKey,
                                    kSecValueData as String: password]

        let status = SecItemAdd(query as CFDictionary, nil)
        switch status {
        case errSecSuccess:
            break
        case errSecDuplicateItem:
            try self.update(username: username, password: password)
        default:
            throw KeychainError.unhandledError(status: status)
        }
    }

    private func update(username: String, password: Data) throws {

        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                    kSecAttrAccount as String: username,
                                    kSecAttrServer as String: Keychain.serverKey]

        let update: [String: Any] = [kSecAttrAccount as String: username]

        let status = SecItemUpdate(query as CFDictionary, update as CFDictionary)
        guard status == errSecSuccess else { throw KeychainError.unhandledError(status: status) }
    }
}

