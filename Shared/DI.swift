//
//  DI.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import Foundation

struct AppDependencies: HasLoginService, HasKeyChainService, HasGithubAPIService {
    private init() { }

    var loginService: LoginService = MockLoginService(result: .success(()))
    var keychain: KeychainService = Keychain()
    var apiService: GithubAPIService = GithubAPIClient()
    static let shared = AppDependencies()
}


struct AppMockedDependencies: HasLoginService, HasKeyChainService, HasGithubAPIService {
    private init() { }

    var loginService: LoginService = MockLoginService(result: .success(()))
    var keychain: KeychainService = MockKeyChainService()
    var apiService: GithubAPIService = MockGithubService()
    static let shared = AppMockedDependencies()
}
