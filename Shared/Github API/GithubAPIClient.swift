//
//  GithubAPIClient.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import Foundation
import Combine

// See: https://docs.github.com/en/rest/reference/search#search-repositories
final class GithubAPIClient {

    // MARK: - Errors
    private struct InvalidLinkError: Error { }

    // MARK: - iVar
    private let decoder: JSONDecoder = {

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
    private let session: URLSession

    // MARK: - Init

    ///:nodoc:
    init(session: URLSession = .shared) {
        self.session = session
    }

    // MARK: - Constants
    static let baseURL = URL(string: "https://api.github.com/search/repositories")!
    static let acceptHeader = "application/vnd.github.v3+json"

    // MARK: - Helpers
    static func searchURL(query: String, page: Int, count: Int) -> URL {

        var components = URLComponents(url: GithubAPIClient.baseURL, resolvingAgainstBaseURL: true)!

        components.queryItems = [
            URLQueryItem(name: "page", value: "\(page)"),
            URLQueryItem(name: "per_page", value: "\(count)"),
            URLQueryItem(name: "q", value: query)
        ]

        return components.url!
    }

    // MARK: - API
    func search(query: String, page: Int, count: Int) -> AnyPublisher<GithubSearchResult, Error> {

        print("Staring query: \(query)")
        guard !query.isEmpty else {
            let result = GithubSearchResult(totalCount: 0, incompleteResults: false, items: [])
            return Just(result).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        let url = GithubAPIClient.searchURL(query: query, page: page, count: count)
        return httpCall(url: url)
    }
    
    func httpCall(url: URL) -> AnyPublisher<GithubSearchResult, Error> {
        return self.session.dataTaskPublisher(for: url)
            .tryMap { response in
                let httpResponse = response.response as! HTTPURLResponse
                let data = response.data
                logger.debug("Response: \(httpResponse.statusCode) - Data Count: \(data.count)")

                // Retrieve data
                let result =  try self.decoder.decode(GithubSearchResult.self, from: data)
               return result

            }.eraseToAnyPublisher()
    }

    static func parseHypermediaLink(_ link: String) throws -> [String: URL] {

        try link.split(separator: ",").reduce(into: [String: URL]()) { (acc, substring) in

            let pair = substring.split(separator: ";")
            guard pair.count == 2 else { throw InvalidLinkError() }

            // Link
            let linkString = pair[0].trimmingCharacters(in: .linkTrimmerSet)
            guard let url = URL(string: linkString) else { throw InvalidLinkError() }

            // Relation
            let relation = pair[1].trimmingCharacters(in: .whitespaces)
            let set =  CharacterSet(charactersIn: "\"")
            guard let first = relation.rangeOfCharacter(from: set) else { throw InvalidLinkError() }
            guard let second = relation[first.upperBound...].rangeOfCharacter(from: set) else { throw InvalidLinkError() }

            let label = String(relation[first.upperBound..<second.lowerBound])
            acc[label] = url
        }
    }
}

private extension CharacterSet {
    static let linkTrimmerSet = CharacterSet.whitespaces.union(CharacterSet(charactersIn: "<>"))
}
