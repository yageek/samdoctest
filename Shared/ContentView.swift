//
//  ContentView.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import SwiftUI

struct ContentView: View {

    // MARK: - States and viewmodel
    @ObservedObject var userStatus: UserStatus

    var body: some View {
        ZStack {
            if userStatus.isLogged {
                MainTabView()
            } else {
                LoginView(userStatus: userStatus)
            }
        }
    }
}
