//
//  TextFieldModifier.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 11.04.21.
//

import SwiftUI

// MARK: - TextField
struct CommonTextField: ViewModifier {
    func body(content: Content) -> some View {
        content.padding()
            .background(Color("textfieldBack"))
            .cornerRadius(5.0)
            .padding(.bottom, 20)
    }
}

// MARK: - Buttons
struct CommonButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        return MyButton(configuration: configuration)
    }

    private struct MyButton: View {
            let configuration: ButtonStyle.Configuration
            @Environment(\.isEnabled) private var isEnabled: Bool
            var body: some View {
                configuration.label
                    .padding(14.0)
                    .background(configuration.isPressed ? Color.primary :  Color("samdocblue"))
                    .foregroundColor(configuration.isPressed ?  Color("samdocblue") : Color.white)
                    .cornerRadius(7.0)
                    .opacity(isEnabled ? 1.0 : 0.5)
            }
        }
}
