//
//  SamDocTestApp.swift
//  Shared
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import SwiftUI
import OSLog
// MARK: - DI

let logger = Logger(subsystem: "samdoc.api", category: "utils")

@main
struct SamDocTestApp: App {

    // MARK: - State Objects
    @StateObject var userStatus = UserStatus()

    var body: some Scene {
        WindowGroup {
            ContentView(userStatus: userStatus)            
        }
    }
}
