//
//  Repository.swift
//  SamDocTest (iOS)
//
//  Created by Yannick Heinrich on 07/04/2021.
//

import Foundation

/// The model object representing a Github repository
struct Repository: Identifiable {
    // MARK: - iVar
    /// :nodoc:
    let id: Int

    /// :nodoc:
    let name: String

    /// :nodoc:
    let stars: Int

    /// :nodoc:
    let author: String

    /// :nodoc:
    let description: String

    /// :nodoc:
    let license: String

    /// :nodoc:
    let issues: Int

    /// :nodoc:
    let forks: Int

    // MARK: - Init
    /// :nodoc:
    init(id: Int, name: String, stars: Int, author: String, description: String = "No description", license: String = "Propriértaire", forks: Int = 0, issues: Int = 0) {
        self.id = id
        self.name = name
        self.stars = stars
        self.author = author
        self.description = description
        self.license = license
        self.forks = forks
        self.issues = issues
    }
}

// MARK: - Convenience initializer
/// :nodoc:
extension Repository {

    /// :nodoc:
    init(from remote: Item) {

        self.id = remote.id
        self.name = remote.name ?? "??"
        self.stars = remote.stargazersCount ?? -1
        self.author = remote.owner?.login ?? "??"
        self.description = remote.itemDescription ?? ""
        self.license = remote.license?.name ?? "Propriétaire"
        self.issues = remote.openIssuesCount ?? -1
        self.forks = remote.forks ?? -1
    }
}



