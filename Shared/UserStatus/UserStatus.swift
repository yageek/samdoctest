//
//  UserStatus.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import Foundation

/// Simple state object to fake the session status
final class UserStatus: ObservableObject {

    // MARK: - iVar
    @Published private(set) var isLogged: Bool

    // MARK: - Init

    /// :nodoc:
    init() {
        self.isLogged = false
    }

    // MARK: - API
    func setUserCredential() {
        self.isLogged = true
    }

    func clearUserCredential() {
        self.isLogged = false
    }
}
