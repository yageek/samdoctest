//
//  MainTabView.swift
//  SamDocTest
//
//  Created by Yannick Heinrich on 09.04.21.
//

import SwiftUI


struct MainTabView: View {

    // MARK: - Body
    var body: some View {
        TabView {

            // List tab
            ListView()
                .tabItem {
                    Image(systemName: "list.star")
                    Text("Github")
                }.tag(1)

            // ARKit
            VRPermissionView()
                .tabItem {
                    Image(systemName: "camera.metering.matrix")
                    Text("ARKit View")

                }.tag(2)
        }
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
