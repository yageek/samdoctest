//
//  SamDocTestTests.swift
//  SamDocTestTests
//
//  Created by Yannick Heinrich on 10.04.21.
//

import XCTest
@testable import SamDocTest

class SamDocTestTests: XCTestCase {

    func testParseHypermedia() throws {

        let link = """
<https://api.github.com/search/repositories?page=2&per_page=10&q=Swift>; rel="next", <https://api.github.com/search/repositories?page=100&per_page=10&q=Swift>; rel="last"
""";

        var value: [String: URL]!
        XCTAssertNoThrow(value = try! GithubAPIClient.parseHypermediaLink(link))
        XCTAssertEqual([
                        "next": URL(string: "https://api.github.com/search/repositories?page=2&per_page=10&q=Swift"),
                        "last": URL(string: "https://api.github.com/search/repositories?page=100&per_page=10&q=Swift")
        ], value)
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testTotalLink() throws {
        let link = """
        <https://api.github.com/search/repositories?page=1&per_page=10&q=Swift>; rel=\"prev\", <https://api.github.com/search/repositories?page=3&per_page=10&q=Swift>; rel=\"next\", <https://api.github.com/search/repositories?page=100&per_page=10&q=Swift>; rel=\"last\", <https://api.github.com/search/repositories?page=1&per_page=10&q=Swift>; rel=\"first\"
        """;

        var value: [String: URL]!
        XCTAssertNoThrow(value = try! GithubAPIClient.parseHypermediaLink(link))
        XCTAssertEqual(value["next"], URL(string: "https://api.github.com/search/repositories?page=3&per_page=10&q=Swift"))

    }
}
